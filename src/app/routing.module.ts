import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home.component';
import { RoomsComponent } from './components/rooms.component';
import { AdditionsComponent } from './components/additions.component';
import { ConfirmationComponent } from './components/confirmation.component';
import { PaymentOnlineComponent } from './components/payment-online.component';
import { PaymentTransferComponent } from './components/payment-transfer.component';
import { PaymentBankComponent } from './components/payment-online-bank.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' }
];

const childs: Routes = [
    { path: 'nf', children: [
        { path: '', component: HomeComponent },
        { path: 'rooms/:from/:to', component: RoomsComponent },
        { path: 'additions', component: AdditionsComponent },
        { path: 'confirmation', component: ConfirmationComponent },
        { path: 'payment/online', component: PaymentOnlineComponent },
        { path: 'payment/online/bank/:bank', component: PaymentBankComponent },
        { path: 'payment/transfer', component: PaymentTransferComponent }
    ]},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}), RouterModule.forChild(childs)],
  exports: [ RouterModule ]
})

export class RoutingModule {}
