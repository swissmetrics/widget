import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';
import { CalculationPrices } from '../models/calculation.model';
import { BookingModel } from '../models/booking.model';

@Component({
    templateUrl: '../templates/payment-online.html',
    styleUrls: ['../style/payments.scss']
})

export class PaymentOnlineComponent {

    public activeBank: string;
    public currency: string;
    public response: BookingModel;
    public prices: CalculationPrices;

    constructor(
        private paramsService: ParamsService,
        private configService: ConfigService,
        private sanitizer: DomSanitizer,
        private router: Router) {
            this.prices = this.paramsService.bookingPrices;
            this.currency = this.configService.config.system_currency;
            this.response = this.paramsService.bookingRet;
            if (!this.response) {
                this.router.navigate(['/']);
            }
    }

    // choosen bank
    pickBank(bank: string): void {
        this.activeBank = bank;
    }

    // go to the bank
    goBank() {
        if (this.activeBank) {
            this.router.navigate([`nf/payment/online/bank/${this.activeBank}`]);
        }
    }

    // return url to icon
    getIcon(img: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${img})`);
    }
}
