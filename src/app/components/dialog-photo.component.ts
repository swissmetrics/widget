import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigService } from '../services/config.service';

@Component({
    templateUrl: '../templates/dialog-photo.html',
    styleUrls: ['../style/dialog.scss'],
})

export class PhotoDialogComponent implements OnInit {

    public photo;

    constructor(
        private configService: ConfigService,
        private sanitizer: DomSanitizer,
        private dialogRef: MatDialogRef<PhotoDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {
        this.photo = this.getPhoto(this.data.image);
    }

    // close modal
    close(): void {
        this.dialogRef.close();
    }

    // return url to image
    getPhoto(img: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${this.configService.getPhoto(img, 'w1200-h1200')})`);
    }

}
