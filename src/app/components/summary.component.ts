import { Component, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';
import { CalculationsDialogComponent } from './dialog-calculations.component';
import { DatePipe } from '@angular/common';
import { ConfigModel } from '../models/config.model';
import { CalculationModel, CalculationPrices } from '../models/calculation.model';
import { ReservationService } from '../services/reservation.service';
import { OtherService } from '../services/other.service';

@Component({
    selector: 'summary',
    templateUrl: '../templates/summary.html',
    styleUrls: ['../style/summary.scss']
})

export class SummaryComponent implements AfterViewInit, OnDestroy {

    @Output() showProgress = new EventEmitter();
    @Output() allPrices = new EventEmitter();
    public pick: any;
    public config: ConfigModel;
    public calculation: CalculationModel;
    public currency: string;
    public addedAdditions: number[] = [];
    public subCode: Subscription;
    private promoCode: string;
    private params: string = '';
    private type: string;
    private inParams: any;
    public allprices: CalculationPrices = {stay_price: 0, additions_price: 0, installment: 0, r_grand_total:0};

    constructor(
        private dialog: MatDialog,
        public configService: ConfigService,
        private router: Router,
        private reservationService: ReservationService,
        private otherService: OtherService,
        private paramsService: ParamsService) {
            this.pick = this.paramsService.choosenRoom;
            this.config = this.configService.config;
            this.currency = this.configService.config.system_currency;
            // user input a rabat code
            this.subCode = this.otherService.getMessage().subscribe(rabat => {
                this.promoCode = rabat.code;
                this.init();
            });
            if (!this.pick) {
                this.router.navigate(['/']);
            }
    }

    ngAfterViewInit() {
        this.init();
    }

    init() {
        this.allprices = {stay_price: 0, additions_price: 0, installment: 0, r_grand_total:0};
        this.calculateParams();
        if (this.pick.rooms.length == 1) {
            this.calculateOne();
            this.type = 'one';
        } else if (this.pick.rooms.length > 1){
            this.calculateMany(this.pick.rooms.length);
            this.type = 'many';
        } else {
            this.router.navigate(['/']);
        }
    }

    calculateParams() {
        if (this.configService.urlSettings.calculateFromUrl) {
            this.inParams = {
                roomId: this.configService.urlSettings.room,
                configId: this.configService.urlSettings.configId,
                from: this.configService.urlSettings.dateFrom,
                to: this.configService.urlSettings.dateTo,
                adults: this.configService.urlSettings.adultCount,
                people: this.pick.search.people[0].age,
                package: this.configService.urlSettings.package
            }
        }
        else {
            this.inParams = {
                roomId: this.pick.rooms[0].room.id,
                configId: Object.keys(this.pick.rooms[0].room.price)[0],
                from: this.pick.search.from,
                to: this.pick.search.to,
                adults: this.pick.search.adults,
                people: this.pick.search.people[0].age,
                package: this.pick.package ? this.pick.package.id : ''
            }
        }
    }

    // call api for one room
    calculateOne() {
        this.showProgress.emit(true);
        this.params = `id=${this.inParams.roomId}&config_id=${this.inParams.configId}`;
        this.params += `&date_from=${this.inParams.from}&date_to=${this.inParams.to}`;
        this.params += `&adult_count=${this.inParams.adults}`;
        this.params += this.paramsService.makeChildrenUrl(this.inParams.people);
        this.params += this.paramsService.makeAdditionsUrl(this.paramsService.choosenAddictions);
        this.params += this.promoCode ? `&promo_card=${this.promoCode}` : '';
        this.params += this.inParams.package ? `&package=${this.inParams.package}` : '';

        this.reservationService
            .calculateOne(this.params)
            .subscribe(ret => {
                for (let key in ret.prices) {
                    this.allprices[key] = ret.prices[key];
                }
                this.paramsService.bookingUrl = this.params;
                this.paramsService.bookingPrices = this.allprices;
                this.showProgress.emit(false);
                this.allPrices.emit({prices: this.allprices, currency: this.currency});
            },
            error => {
                this.router.navigate(['/']);
            }
        );
    }

    // call api for several rooms
    calculateMany(group: number) {
        this.showProgress.emit(true);
        let params = '';
        for (let g = 0; g < group; g++) {
            let id = this.pick.rooms[g].room.id;
            let configId = Object.keys(this.pick.rooms[g].room.price)[0];
            params += `rooms[${g}][id]=${id}&rooms[${g}][config_id]=${configId}&`;
            params += `rooms[${g}][date_from]=${this.inParams.from}&rooms[${g}][date_to]=${this.inParams.to}&`;
            params += `rooms[${g}][adult_count]=${this.pick.search.people[g].adults}`;
            params += this.paramsService.makeChildrenUrl(this.pick.search.people[g].age, g);
            params += this.paramsService.makeAdditionsUrl(this.paramsService.choosenAddictions, g);
            params += this.promoCode ? `&rooms[${g}][promo_card]=${this.promoCode}` : '';
            params += this.inParams.package ? `&rooms[${g}][package]=${this.inParams.package}` : '';
            params += (g < group - 1) ? '&' : '';
        }
        this.params = params;

        this.reservationService
            .calculateMany(this.params)
            .subscribe(ret => {
                for (let r of ret) {
                    for (let key in r.prices) {
                        this.allprices[key] += parseFloat(r.prices[key]);
                    }
                }
                this.paramsService.bookingUrl = this.params;
                this.paramsService.bookingPrices = this.allprices;
                this.showProgress.emit(false);
                this.allPrices.emit({prices: this.allprices, currency: this.currency});
            },
            error => {
                this.router.navigate(['/']);
            }
        );
    }

    // when user add an Addition
    updateAdditions(additions: number[]) {
        this.paramsService.choosenAddictions = additions;
        this.init();
    }

    ngOnDestroy() {
        this.subCode.unsubscribe();
    }

    openCalculations() {
        this.dialog.open(CalculationsDialogComponent, {
            panelClass: 'calculations-modal',
            data: {
                params: this.params,
                type: this.type,
                calculation: this.allprices
            }
        });
    }
}
