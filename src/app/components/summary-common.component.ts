import { Component, Input, ViewChild } from '@angular/core';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';
import { SummaryComponent } from './summary.component';
import { CalculationPrices } from '../models/calculation.model';

@Component({
    selector: 'summary-common',
    templateUrl: '../templates/summary-common.html',
    styleUrls: ['../style/summary.scss']
})

export class SummaryCommonComponent {

    @Input() innerWidth: number;
    @ViewChild(SummaryComponent) summaryComponent: SummaryComponent;
    public pick: any;
    public showProgress: boolean = true;
    public allPrice: {prices: CalculationPrices, currency: string};

    constructor(private paramsService: ParamsService, private configService: ConfigService) {
        this.pick = this.paramsService.choosenRoom;
    }

    // pass prices from child component
    onAllPrices(e) {
        this.allPrice = e;
    }

    // if show progress bar
    onShowProgress(e) {
        this.showProgress = e;
    }

    // when user add an Addition
    updateAdditions(additions: number[]) {
        this.summaryComponent.updateAdditions(additions);
    }
}
