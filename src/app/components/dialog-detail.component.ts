import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigService } from '../services/config.service';
import { PhotoDialogComponent } from './dialog-photo.component';

@Component({
    templateUrl: '../templates/dialog-room-detail.html',
    styleUrls: ['../style/dialog.scss'],
})

export class DetailDialogComponent implements OnInit {

    public currentIndexPhoto: number = 0;
    public galleryPhoto;
    public openDesc: boolean = false;

    constructor(
        private configService: ConfigService,
        private sanitizer: DomSanitizer,
        private dialog: MatDialog,
        private dialogRef: MatDialogRef<DetailDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {
        this.galleryPhoto = this.getPhoto(this.data.roomDesc.photos[0]);
    }

    // close modal
    close(): void {
        this.dialogRef.close(false);
    }

    // make thousend separator
    formatPrice(price: string): string {
        return this.configService.formatPrice(price);
    }

    // return url to image
    getPhoto(img: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${this.configService.getPhoto(img, 'w900-h550-c')})`);
    }

    // show next/prev photo in gallery
    slidePhoto(direction: number) {
        let noPhotos = this.data.roomDesc.photos.length;
        this.currentIndexPhoto += direction;
        if (this.currentIndexPhoto == noPhotos) {
            this.currentIndexPhoto = 0;
        }
        else if (this.currentIndexPhoto < 0) {
            this.currentIndexPhoto = noPhotos - 1;
        }
        this.galleryPhoto = this.getPhoto(this.data.roomDesc.photos[this.currentIndexPhoto]);
    }

    // show image in full size
    fullSizeImage() {
        this.dialog.open(PhotoDialogComponent, {
            height: '650px',
            panelClass: 'photo-modal',
            data: {
                image: this.data.roomDesc.photos[this.currentIndexPhoto]
            }
        });
    }

    addRoom() {
        this.dialogRef.close(true);
    }

}
