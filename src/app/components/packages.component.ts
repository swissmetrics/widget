import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { PackagesService } from '../services/packages.service';
import { ConfigService } from '../services/config.service';
import { ParamsService } from '../services/params.service';
import { RoomsService } from '../services/rooms.service';
import { PackageModel, PackageSearchParams } from '../models/package.model';
import { MealsModel } from '../models/meals.model';
import { RoomsModel } from '../models/rooms.model';
import { PackageDialogComponent } from './dialog-package.component';

@Component({
    selector: 'packages',
    templateUrl: '../templates/packages.html',
    styleUrls: ['../style/packages.scss']
})

export class PackagesComponent implements OnInit {

    @Input() meals: MealsModel[];
    @Input() mealFilter: number[];
    @Input() packageParams: PackageSearchParams;
    @Input() activeTab: number;
    @Input() paramsUrl: any;
    @Output() selectedPackage = new EventEmitter();

    public packages: PackageModel[];
    private routeSub: any;
    public error: string | boolean;
    public showProgress: boolean = true;
    public currency: string;
    private peopleUrl: string = '1';
    private packageUrl: string;
    public lowerPrice: {id: number, price: number}[] = [];
    public checkedPrices: number[] = [];
    public errorPrices: {id: number, type: string}[] = [];
    public errorPricesArray: number[] = [];

    constructor(
        private packagesService: PackagesService,
        private sanitizer: DomSanitizer,
        private router: Router,
        private dialog: MatDialog,
        private paramsService: ParamsService,
        private roomsService: RoomsService,
        private configService: ConfigService) {
            this.currency = this.configService.config.system_currency;
    }

    ngOnInit() {
        this.showProgress = true;
        this.packages = null;
        this.error = null;
        this.peopleUrl = this.paramsUrl['people'];
        this.packageUrl = this.paramsUrl['package'];
        this.getPackages(this.paramsUrl['from'], this.paramsUrl['to']);
    }

    // get available packages
    getPackages(from: string, to: string) {
        this.packagesService
            .packages(`date_from=${from}&date_to=${to}&lang_id=${this.configService.language.id}`)
            .subscribe(
                ret => {
                    this.packages = ret;
                    this.currentPackage();
                    this.tabActive(this.activeTab);
                },
                error => this.error = error
            );
    }

    // return url to image
    getPhoto(img: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${this.configService.getPhoto(img, 'w400-c')})`);
    }

    // show rooms from the package
    showRooms(pId) {
        let currentUri = this.router.url.replace(/;package=.*/g, '');
        currentUri = currentUri.replace(/;people=.*/g, '');
        this.router.navigate([currentUri, {people: this.peopleUrl, package: pId, rnd: Math.random()}]);
    }

    // display current package box in rooms on the top
    currentPackage() {
        if (this.packageUrl) {
            this.selectedPackage.emit(this.packages);
        }
    }

    // use meal filter
    useMealFilter(meals) {
        if (this.mealFilter.length === 0) {
            return true;
        }
        for (let meal of meals) {
            if (this.mealFilter.indexOf(meal) != -1) {
                return true;
            }
        }
        return false;
    }

    // modal of package
    openPackageDialog(packageDetail: PackageModel): void {

        let dialogRef = this.dialog.open(PackageDialogComponent, {
            width: '80%',
            panelClass: 'room-modal',
            data: {
                package: packageDetail,
                meals: this.meals,
                lowerPrice: this.lowerPrice,
                errorPrices: this.errorPrices,
                checkedPrices: this.checkedPrices
            }
        });

        dialogRef.afterClosed().subscribe( result => {
            if (result) {
                this.showRooms(result);
            }
        });
    }

    // find the lowest price
    lowestPrice() {
        for (let p of this.packages) {
            if ( !this.checkAllowedDays(p) ){
                this.getAvailableRooms(p);
            }
        }
    }

    // check errors before send request
    checkAllowedDays(p: PackageModel): boolean {
        if (this.errorPricesArray.indexOf(p.id) !== -1) {
            return true;
        }
        if (p.days_type == 1 && this.paramsService.daysStay != p.days) {
            this.errorPrices.push({id: p.id, type: 'equal'});
            this.errorPricesArray.push(p.id);
            return true;
        }
        else if (p.days_type == 2 && this.paramsService.daysStay < p.days) {
            this.errorPrices.push({id: p.id, type: 'min'});
            this.errorPricesArray.push(p.id);
            return true;
        }
        else if ( this.allowedWeekDays(p) ) {
            this.errorPrices.push({id: p.id, type: 'arrival'});
            this.errorPricesArray.push(p.id);
            return true;
        }
        return false;
    }

    // calculate allowed days for package
    allowedWeekDays(p: PackageModel): boolean {
        let arrive = new Date(this.paramsService.searchParams.from).getDay();

        if (p.allowed_arrival_week_days.length == 0) {
            return false;
        }
        if (p.allowed_arrival_week_days.indexOf(arrive) == -1) {
            return true;
        }

        return false;
    }

    // room in the package
    getAvailableRooms(p: PackageModel) {
        if (this.packageParams.rooms === 1) {
            let params = `package=${p.id}&date_from=${this.packageParams.from}&date_to=${this.packageParams.to}&adult_count=${this.packageParams.adult}`;
            params += this.paramsService.makeChildrenUrl(this.packageParams.age);
            this.roomsService
                .roomPackageAvailability(params)
                .subscribe(
                    ret => {
                        let min = this.findMinPrice(ret);
                        this.lowerPrice.push({id: p.id, price: min});
                        this.checkedPrices.push(p.id);
                    },
                    error => {
                        this.errorPrices.push({id: p.id, type: 'server'});
                        this.errorPricesArray.push(p.id);
                    }
            );
        } else {
            let params: string = '';
            for (let g = 0; g < this.packageParams.rooms; g++) {
                params += `rooms[${g}][date_from]=${this.packageParams.from}&rooms[${g}][date_to]=${this.packageParams.to}&rooms[${g}][adult_count]=${this.packageParams.adult[g]}`;
                params += this.paramsService.makeChildrenUrl(this.packageParams.age[g], g);
                params += `&rooms[${g}][package]=${p.id}`;
                params += (g < this.packageParams.rooms - 1) ? '&' : '';
            }
            this.roomsService
                .roomsPackageAvailability(params)
                .subscribe(
                    ret => {
                        let min = this.findMinPrice(ret[0]);
                        this.lowerPrice.push({id: p.id, price: min});
                        this.checkedPrices.push(p.id);
                    },
                    error => {
                        this.errorPrices.push({id: p.id, type: 'server'});
                        this.errorPricesArray.push(p.id);
                    }
            );
        }
    }

    findMinPrice(prices: RoomsModel[]) {
        let allPrices: number[] = [];
        for (let p of prices) {
            allPrices.push(parseInt(p.price[Object.keys(p.price)[0]].price));
        }
        return Math.min(...allPrices);
    }

    // make thousend separator
    formatPrice(price: string): string {
        return this.configService.formatPrice(price);
    }

    // load prices when Package Tab is active
    tabActive(tab: number) {
        if (tab === 1 && this.lowerPrice.length === 0) {
            this.lowestPrice();
        }
    }
}
