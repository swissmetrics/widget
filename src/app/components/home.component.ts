import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ConfigService } from '../services/config.service';
import { PreferencesModel } from '../models/preferences.model';

@Component({
    selector: 'app-comment',
    templateUrl: '../templates/home.html',
    styleUrls: ['../style/home.scss']
})

export class HomeComponent implements OnDestroy {

    public subPreferences: Subscription;
    public preferences: PreferencesModel;

    constructor(private configService: ConfigService) {
        this.preferences = this.configService.preferences;
        this.subPreferences = this.configService.pullPreferences().subscribe(preferences => {
            this.preferences = preferences;
        });
    }

    ngOnDestroy() {
        this.subPreferences.unsubscribe();
    }
}
