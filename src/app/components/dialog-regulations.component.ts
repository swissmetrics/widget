import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfigService } from '../services/config.service';
import { OtherService } from '../services/other.service';
import { RegulationsModel } from '../models/other.model';

@Component({
    templateUrl: '../templates/dialog-regulations.html',
    styleUrls: ['../style/dialog.scss'],
})

export class RegulationsDialogComponent implements OnInit {

    public content: RegulationsModel;

    constructor(
        private configService: ConfigService,
        private otherService: OtherService,
        private dialogRef: MatDialogRef<RegulationsDialogComponent>) {
    }

    ngOnInit() {
        this.otherService
            .getRegulations(`lang_id=${this.configService.language.id}`)
            .subscribe(ret => this.content = ret);
    }

    // close modal
    close(): void {
        this.dialogRef.close();
    }

}
