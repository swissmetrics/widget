import { Component, Input, Output, EventEmitter, Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { RoomsModel } from '../models/rooms.model';
import { RoomsDescModel } from '../models/roomsdesc.model';
import { EquipmentModel } from '../models/equipment.model';
import { ConfigService } from '../services/config.service';
import { DetailDialogComponent } from './dialog-detail.component';

@Component({
    selector: 'room',
    templateUrl: '../templates/room.html',
    styleUrls: ['../style/room.scss']
})

export class RoomComponent {

    @Input() roomDetail: RoomsModel;
    @Input() roomDesc: RoomsDescModel;
    @Input() equipments: {};
    @Input() eqFilter: string[];
    @Output() selectedRoom = new EventEmitter();

    public currency: string;

    constructor(
        private dialog: MatDialog,
        private sanitizer: DomSanitizer,
        private configService: ConfigService) {
        this.currency = this.configService.config.system_currency;
    }

    // return url to image
    getPhoto(img: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${this.configService.getPhoto(img, 'w400-c')})`);
    }

    // make thousend separator
    formatPrice(price: string): string {
        return this.configService.formatPrice(price);
    }

    // add room to basket
    addRoom(): void {
      this.emitAddRoom(this.roomDetail);
    }

    // emit event to parent component
    emitAddRoom(e): void {
        this.selectedRoom.emit(e);
    }

    // use equipment filter
    useEquipmentFilter(): boolean {
        if (!this.eqFilter.length) {
            return true;
        }
        for (let e of this.roomDesc.equipment) {
            if (this.eqFilter.indexOf(e.id) !== -1) {
                return true;
            }
        }
        return false;
    }

    // modal of detail
    openDetailDialog(): void {

        let dialogRef = this.dialog.open(DetailDialogComponent, {
            panelClass: 'room-modal',
            data: {
                roomDetail: this.roomDetail,
                roomDesc: this.roomDesc,
                equipments: this.equipments,
                currency: this.currency
            }
        });

        dialogRef.afterClosed().subscribe( result => {
            if (result) {
                this.emitAddRoom(this.roomDetail);
            }
        });
    }

}
