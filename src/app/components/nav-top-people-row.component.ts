import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';

@Component({
    selector: 'people-row',
    templateUrl: '../templates/nav-top-people-row.html',
    styleUrls: ['../style/nav-top-people-row.scss']
})

export class NavTopPeopleRowComponent {

    @Input('group') rowForm: FormGroup;
    @Output() changedPeople = new EventEmitter();

    public kidsAge: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
    public amountKids: number[] = [];
    public adults: number[] = [];
    public children: number[] = [];

    constructor(
        private paramsService: ParamsService,
        private configService: ConfigService) {
            setTimeout(() => {
                for (let i = 1; i <= this.configService.config.max_adult_count; i++) {
                    this.adults.push(i);
                }
                for (let i = 0; i <= this.configService.config.max_children_count; i++) {
                    this.children.push(i);
                }
            }, 0);
    }

    ngOnInit() {
        this.setKidsNumber({value: this.rowForm.controls.kids.value})
    }

    // after selecting children
    public setKidsNumber(event) {
        this.amountKids = [];
        for(let i = 0; i < event.value; i++) {
            this.amountKids.push(i)
        }
    }

    public onChangePeople() {
        this.changedPeople.emit();
    }
}
