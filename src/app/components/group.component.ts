import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';

@Component({
    selector: 'group',
    templateUrl: '../templates/group.html',
    styleUrls: ['../style/group.scss'],
    providers: [CurrencyPipe]
})

export class GroupComponent implements OnInit{

    @Output() selectedGroup = new EventEmitter();
    @Input() isPackage: number;

    public activeGroup: number = 0;
    public content = [];
    public people = [];
    public currency: string;
    public showWarning: boolean;
    public compleated: boolean;

    constructor(
        private paramsService: ParamsService,
        private configService: ConfigService,
        private route: ActivatedRoute,
        private router: Router,
        private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.content = [];
            this.compleated = false;
            this.showWarning = false;
        });
    }

    setGroupNo(value) {
        this.people = this.paramsService.searchParams['people'];
        this.currency = this.configService.config['system_currency'];
    }

    // set active group
    setActiveGroup(order: number) {
        this.activeGroup = order;
        this.selectedGroup.emit(order);
    }

    // passed selected room params
    passParams(roomParams) {
        this.content[this.activeGroup] = roomParams;
        this.showWarning = true;
        // all rooms were choosen
        if ((this.people.length <= this.content.length) && !this.content.includes(undefined)) {
            this.compleated = true;
            this.showWarning = false;
            window.scrollTo(0, 0);
        }
        else {
            this.passFocus();
        }
    }

    // set focus on the next card
    passFocus() {
        for (let i in this.people) {
            if (!this.content[i]) {
                this.setActiveGroup(+i);
                break;
            }
        }
    }

    // return url to image
    getPhoto(img: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${this.configService.getPhoto(img, 'w400-c')})`);
    }

    // make thousend separator
    formatPrice(price: string) {
        return this.configService.formatPrice(price);
    }

    // close warning - select next room
    closeWarning() {
        this.showWarning = false;
    }

    // next step - buying
    buyGroup() {
        this.paramsService.choosenRoom = {
            rooms: this.content,
            search: this.paramsService['searchParams'],
            package: this.isPackage
        };
        this.router.navigate(['nf/additions']);
    }
}
