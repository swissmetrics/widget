import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BookingModel } from '../models/booking.model';
import { CalculationPrices } from '../models/calculation.model';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';

@Component({
    templateUrl: '../templates/payment-transfer.html',
    styleUrls: ['../style/payments.scss']
})

export class PaymentTransferComponent {

    public inadvance: number;
    public currency: string;
    public allowedPayment: number;
    public response: BookingModel;
    public prices: CalculationPrices;

    constructor(
        private paramsService: ParamsService,
        private configService: ConfigService,
        private router: Router
    ) {
        this.prices = this.paramsService.bookingPrices;
        this.currency = this.configService.config.system_currency;
        this.allowedPayment = this.configService.config.payment_type;
        this.response = this.paramsService.bookingRet;
        if (!this.response) {
            this.router.navigate([`/`]);
        }
    }

}
