import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PhotoDialogComponent } from './dialog-photo.component';
import { CalculationModel, CalculationExtModel } from '../models/calculation.model';
import { ConfigService } from '../services/config.service';
import { ParamsService } from '../services/params.service';
import { ReservationService } from '../services/reservation.service';
import { OtherService } from '../services/other.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    templateUrl: '../templates/dialog-calculations.html',
    styleUrls: ['../style/calculations.scss'],
})

export class CalculationsDialogComponent implements OnInit {

    public calculation: CalculationExtModel;
    private promoCode: string;
    public currency: string;
    public pick: any;
    public summaryPrices: CalculationModel;
    public sumRoom: number[] = [];
    private type: string;
    public subCode: Subscription;

    constructor(
        private configService: ConfigService,
        private paramsService: ParamsService,
        private reservationService: ReservationService,
        private otherService: OtherService,
        private dialogRef: MatDialogRef<CalculationsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
            this.pick = this.paramsService.choosenRoom;
            this.currency = this.configService.config.system_currency;
            this.promoCode = this.otherService.rabatCode;
            this.summaryPrices = data.calculation;
            // user input a rabat code
            this.subCode = this.otherService.getMessage().subscribe(rabat => {
                this.close();
            });
    }

    ngOnInit() {
        if (this.data.type === 'one') {
            this.calculateExtOne();
        } else {
            this.calculateExtMany();
        }

    }

    // call api for one room
    calculateExtOne() {
        this.reservationService
            .calculateExtOne(this.data.params)
            .subscribe(ret => {
                this.calculation = ret;
                this.calculateRoomSum('one');
            }
        );
    }

    // call api for several rooms
    calculateExtMany() {
        this.reservationService
            .calculateExtMany(this.data.params)
            .subscribe(ret => {
                this.calculation = ret;
                this.calculateRoomSum('many');
            }
        );
    }

    // close modal
    close(): void {
        this.dialogRef.close();
    }

    // calculate price for day + additions
    calculateDayPrice(dayPrice): number {
        let price = dayPrice.stay_price;
        if (dayPrice['additions']) {
            for (let a of dayPrice['additions']) {
                price += +a.a_price;
            }
        }
        return price;
    }

    // calculate price for all days
    calculateRoomSum(kind: string) {

        if (kind === 'one') {
            this.sumRoom[0] = 0;
            for (let p in this.calculation.prices) {
                this.sumRoom[0] += this.calculation.prices[p].stay_price;
                if (this.calculation.prices[p]['additions']) {
                    for (let a of this.calculation.prices[p]['additions']) {
                        this.sumRoom[0] += +a.a_price;
                    }
                }
            }
        }
        else if (kind === 'many') {
            for (let calc in this.calculation) {
                this.sumRoom[calc] = 0;
                for (let p in this.calculation[calc].prices) {
                    this.sumRoom[calc] += this.calculation[calc].prices[p].stay_price;
                    if (this.calculation[calc].prices[p]['additions']) {
                        for (let a of this.calculation[calc].prices[p]['additions']) {
                            this.sumRoom[calc] += +a.a_price;
                        }
                    }
                }
            }
        }
    }
}
