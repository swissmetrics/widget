import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ConfigService } from '../services/config.service';
import { ConfigModel } from '../models/config.model';

@Component({
  selector: 'app-root',
  templateUrl: '../templates/app.component.html',
  styleUrls: ['../style/app.component.scss']
})

export class AppComponent implements OnInit {

    constructor(
        private configService: ConfigService,
        private route: ActivatedRoute,
    ) {
        let savedLanguage = this.configService.getLanguage();

        if (savedLanguage) {
            this.configService.language.id = parseInt(savedLanguage[1]);
            this.configService.language.short = savedLanguage[0];
        }
    }

    ngOnInit() {
        this.configService
            .getConfig()
            .subscribe(ret => {
                this.configService.config = Object.assign(this.configService.config, ret);
                this.changeLanguage();
            });

        this.configService
            .getPreferences(`lang_id=${this.configService.language.id}`)
            .subscribe(ret => {
                this.configService.preferences = ret;
                this.configService.sharePreferences(ret)
            });
    }

    // change language at the begining
    changeLanguage() {
        let currentLng = this.configService.getLanguage();
        if (!currentLng) {
            if (this.configService.config.languages[0].lg_code != this.configService.defaultLanguage) {
                let lngStr = this.configService.config.languages[0].lg_code + '|' + this.configService.config.languages[0].id;
                this.configService.setLanguage(lngStr);
                window.location.href = window.location.href + '&lng=' + this.configService.config.languages[0].lg_code
            }
        }
    }

}
