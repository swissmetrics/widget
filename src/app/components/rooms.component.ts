import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { RoomsService } from '../services/rooms.service';
import { ConfigService } from '../services/config.service';
import { ParamsService } from '../services/params.service';
import { RoomsModel } from '../models/rooms.model';
import { EquipmentModel } from '../models/equipment.model';
import { MealsModel } from '../models/meals.model';
import { PackageModel, PackageSearchParams } from '../models/package.model';
import { GroupComponent } from './group.component';
import { PackagesComponent } from './packages.component';

@Component({
    templateUrl: '../templates/rooms.html',
    styleUrls: ['../style/rooms.scss']
})

export class RoomsComponent implements OnInit, OnDestroy {

    @ViewChild(GroupComponent) groupComponent: GroupComponent
    @ViewChild(PackagesComponent) packagesComponent: PackagesComponent

    public lastBooking: number = 0;
    public rooms: RoomsModel[] | boolean;
    public roomsGroup: RoomsModel[] | any;
    public meals: MealsModel[];
    public equipment: EquipmentModel[];
    public roomDescTr: object = {};
    public equipmentTr: object = {};
    public roomsTr: object = {};
    public roomsGroupTr: object = {};
    public mealsTr: object = {};
    private routeSub: any;
    private paramsUrl: any;
    public error: string | boolean;
    public showProgress: boolean = true;
    public group: number = 0;
    public isPackage: number = 0;
    public currentPackage: PackageModel | boolean;
    public mealFilter: number[] = [];
    public eqFilter: number[] = [];
    public packageParams: PackageSearchParams;
    public activeTab: number = 0;

    constructor(
        private roomsService: RoomsService,
        private route: ActivatedRoute,
        private router: Router,
        private sanitizer: DomSanitizer,
        private paramsService: ParamsService,
        private configService: ConfigService) {
    }

    ngOnInit() {
        this.getEquipment();
        this.routeSub = this.route.params.subscribe(params => {

            this.paramsUrl = params;
            this.lastBooking = 0;
            this.roomDescription(params['from'], params['to']);
            this.getMeals(params['from'], params['to']);

            this.initSearch(params);
        })
    }

    // control what to search
    initSearch(p) {
        this.rooms = null;
        this.error = null;
        this.showProgress = true;
        let kidsAge = [];
        let adult = [];
        let searchParams = this.paramsService['searchParams'];
        this.group = searchParams.rooms;

        for(let p of searchParams.people) {
            kidsAge.push(p.age);
            adult.push(p.adults);
        }

        this.packageParams = {
            rooms: this.group,
            from: p['from'],
            to: p['to'],
            adult: this.group > 1 ? adult : adult[0],
            age: this.group > 1 ? kidsAge : kidsAge[0]
        };

        if (p['package']) {
            this.isPackage = p['package'];
            this.activeTab = 0;
            // group
            if (this.group > 1) {
                this.getPackageGroupRooms(p['package'], p['from'], p['to'], adult, kidsAge);
            } else {
                this.getPackageRooms(p['package'], p['from'], p['to'], adult[0], kidsAge[0]);
            }
        } else {
            // group
            if (this.group > 1) {
                this.getGroupRooms(p['from'], p['to'], adult, kidsAge);
            } else {
                this.getRooms(p['from'], p['to'], adult[0], kidsAge[0]);
            }
            this.currentPackage = null;
        }
    }

    // get info about last booking
    lastBooked() {
        setTimeout(() => {
            this.roomsService
                .lastBooked()
                .subscribe(
                    ret => {
                        this.lastBooking = Math.round((((new Date()).getTime() / 1000) - ret) / 3600);
                    }
                );
        }, 1500);
    }

    // get available rooms
    getRooms(from: string, to: string, adults: number, kids: number[]) {
        let params = `date_from=${from}&date_to=${to}&adult_count=${adults}`;
        params += this.paramsService.makeChildrenUrl(kids);
        params += this.paramsService.makeMealsUrl(this.mealFilter);
        this.roomsService
            .roomAvailability(params)
            .subscribe(
                ret => {
                    this.rooms = ret;
                    for(let r of ret) {
                        this.roomsTr[r.id] = r;
                    }
                    this.lastBooked();
                    this.afterGettingRooms();
                },
                error => {
                    this.error = error;
                    this.afterGettingRooms();
                }
            );
    }

    // get available rooms for Group
    getGroupRooms(from: string, to: string, adults: number[], kids: any[]) {
        let params: string = '';
        for (let g = 0; g < this.group; g++) {
            params += `rooms[${g}][date_from]=${from}&rooms[${g}][date_to]=${to}&rooms[${g}][adult_count]=${adults[g]}`;
            params += this.paramsService.makeChildrenUrl(kids[g], g);
            params += this.paramsService.makeMealsUrl(this.mealFilter, g);
            params += (g < this.group - 1) ? '&' : '';
        }
        this.roomsService
            .roomsAvailability(params)
            .subscribe(
                ret => {
                    this.roomsGroupTr = ret;
                    this.roomsGroup = [];
                    if (ret.length > 0) {
                        for (let g in ret) {
                            let groupTr = [];
                            for (let r in ret[g]) {
                                groupTr.push(ret[g][r]);
                                this.roomsGroup.push(groupTr);
                            }
                        }
                        this.rooms = this.roomsGroup[0];
                        this.roomsTr = this.roomsGroupTr[0];
                        this.lastBooked();
                        this.afterGettingRooms();
                    }
                },
                error => {
                    this.error = error;
                    this.afterGettingRooms();
                }
            );
    }

    // get available rooms for the package
    getPackageRooms(packageId: number, from: string, to: string, adults: number, kids: number[]) {
        let params = `package=${packageId}&date_from=${from}&date_to=${to}&adult_count=${adults}`;
        params += this.paramsService.makeChildrenUrl(kids);
        params += this.paramsService.makeMealsUrl(this.mealFilter);
        this.roomsService
            .roomPackageAvailability(params)
            .subscribe(
                ret => {
                    this.rooms = ret;
                    for(let r of ret) {
                        this.roomsTr[r.id] = r;
                    }
                    this.lastBooked();
                    this.afterGettingRooms();
                },
                error => {
                    this.error = error;
                    this.afterGettingRooms();
                }
            );
    }

    // get available rooms' group for the package
    getPackageGroupRooms(packageId: number, from: string, to: string, adults: number[], kids: any[]) {
        let params: string = '';
        for (let g = 0; g < this.group; g++) {
            params += `rooms[${g}][date_from]=${from}&rooms[${g}][date_to]=${to}&rooms[${g}][adult_count]=${adults[g]}`;
            params += this.paramsService.makeChildrenUrl(kids[g], g);
            params += `&rooms[${g}][package]=${packageId}`;
            params += this.paramsService.makeMealsUrl(this.mealFilter, g);
            params += (g < this.group - 1) ? '&' : '';
        }
        this.roomsService
            .roomsPackageAvailability(params)
            .subscribe(
                ret => {
                    this.roomsGroupTr = ret;
                    this.roomsGroup = [];
                    if (ret.length > 0) {
                        for (let g in ret) {
                            let groupTr = [];
                            for (let r in ret[g]) {
                                groupTr.push(ret[g][r]);
                                this.roomsGroup.push(groupTr);
                            }
                        }
                        this.rooms = this.roomsGroup[0];
                        this.roomsTr = this.roomsGroupTr[0];
                        let first = Object.keys(ret[0])[0];
                        this.lastBooked();
                        this.afterGettingRooms();
                    }
                },
                error => {
                    this.error = error;
                    this.afterGettingRooms();
                }
            );
    }

    // description of all rooms
    roomDescription(from: string, to: string) {
        this.roomsService
            .roomDescription(`lang_id=${this.configService.language.id}`)
            .subscribe(ret => {
                for(let r of ret) {
                    this.roomDescTr[r.roomstandard_id] = r;
                }
                this.groupComponent.setGroupNo(this.group);
            });
    }

    // available equipment
    getEquipment() {
        this.roomsService
            .equipment(`lang_id=${this.configService.language.id}`)
            .subscribe(
                ret => {
                    this.equipment = ret;
                    for(let r of ret) {
                        this.equipmentTr[r.id] = r;
                    }
                }
            );
    }

    // meals type
    getMeals(from: string, to: string) {
        this.roomsService
            .meals(`date_from=${from}&date_to=${to}`)
            .subscribe(ret => {
                this.meals = ret;
                for(let r of ret) {
                    this.mealsTr[r.id] = r;
                }
            });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

    // detect when user choose room
    onSelectedRoom(e) {
        let p = {
            room: e,
            desc: this.roomDescTr[e.id]
        };
      // group of rooms is active
      if (this.group > 1) {
        this.groupComponent.passParams(p);
      } else {
          this.buyRoom(p);
      }
    }

    // user clicks in group of rooms box
    onSelectedGroup(order) {
        this.rooms = this.roomsGroup[order];
        this.roomsTr = this.roomsGroupTr[order];
    }

    // when user choose package
    onSelectedPackage(packages) {
        for (let p of packages) {
            if (p.id == this.isPackage) {
                this.currentPackage = p;
                break;
            }
        }
    }

    // selct single room
    buyRoom(event) {
        this.paramsService.choosenRoom = {
            rooms: [event],
            search: this.paramsService['searchParams'],
            package: this.currentPackage
        };
        this.router.navigate(['nf/additions']);
    }

    // return url to image
    getPhoto(img: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${this.configService.getPhoto(img, 'w200-c')})`);
    }

    // show all rooms
    removePackage() {
        this.router.navigate(['nf/rooms', this.paramsUrl['from'], this.paramsUrl['to'], {people: this.paramsUrl['people']}]);
    }

    // meal filter
    changeMealFilter(mealId) {
        if (this.mealFilter.indexOf(mealId) != -1) {
            this.mealFilter.splice(this.mealFilter.indexOf(mealId), 1);
        }
        else {
            this.mealFilter.push(mealId);
        }
        this.initSearch(this.paramsUrl);
    }

    // equipment filter
    changeEquipmentFilter(eqId) {
        if (this.eqFilter.indexOf(eqId) != -1) {
            this.eqFilter.splice(this.eqFilter.indexOf(eqId), 1);
        }
        else {
            this.eqFilter.push(eqId);
        }
    }

    // changing tabs
    onTabChange(e) {
        this.activeTab = e.index;
        this.packagesComponent.tabActive(e.index);
    }

    // do it after load room list
    afterGettingRooms() {
        this.showProgress = false;
    }

    // open modal with package from rooms list
    openPackageDialog(packageDetail: PackageModel): void {
        this.packagesComponent.openPackageDialog(packageDetail);
    }
}
