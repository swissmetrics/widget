import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from '../services/config.service';

@Component({
    selector: 'error',
    templateUrl: '../templates/error.html',
    styleUrls: ['../style/error.scss'],
})

export class ErrorComponent implements OnInit {

    @Input() errorMessage: string = '';

    constructor (private configService: ConfigService) {
    }

    ngOnInit() {
    }
}
