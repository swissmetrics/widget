import { Component, Input } from '@angular/core';

@Component({
    selector: 'aditions-steps',
    templateUrl: '../templates/additions-steps.html',
    styleUrls: ['../style/additions-steps.scss']
})

export class AditionsStepsComponent {
    @Input() activeTab: string;
}
