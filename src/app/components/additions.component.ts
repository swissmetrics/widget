import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AdditionsModel } from '../models/additions.model';
import { ConfigService } from '../services/config.service';
import { ParamsService } from '../services/params.service';
import { AdditionsService } from '../services/additions.service';
import { SummaryCommonComponent } from './summary-common.component';

@Component({
    templateUrl: '../templates/additions.html',
    styleUrls: ['../style/additions.scss']
})

export class AdditionsComponent implements OnInit {

    @ViewChild(SummaryCommonComponent) summaryComponent: SummaryCommonComponent;

    public showProgress: boolean = true;
    public additions: AdditionsModel[];
    public error: string;
    public currency: string;
    public additionBasket: number = 0;
    public selectedAdditions: number[] = [];
    public innerWidth: number;
    public allAdditions: number[] = [];

    constructor(
        private configService: ConfigService,
        private paramsService: ParamsService,
        private location: Location,
        private router: Router,
        private additionsService: AdditionsService) {
            this.currency = this.configService.config.system_currency;
    }

    ngOnInit() {
        let params = `lang_id=${this.configService.language.id}`;

        this.additionsService
            .getAdditions(params)
            .subscribe(
                ret => {
                    this.additions = ret;
                    this.showProgress = false;
                    this.ifAreSet();
                },
                error => this.error = error
            );

        this.innerWidth = window.innerWidth;
    }

    // equimpments are set in the url
    ifAreSet() {
        if (this.paramsService.choosenAddictions.length) {
            this.allAdditions = this.paramsService.choosenAddictions;
            for (let add of this.additions['additions']) {
                if (this.allAdditions.indexOf(add.id) !== -1 ) {
                    this.calculatePriceAdditions(add.id, add.a_price);
                }
            }
        }
    }

    // make thousend separator
    formatPrice(price: string): string {
        return this.configService.formatPrice(price);
    }

    // select addition
    addAddition(id: number, price: string) {
        this.calculatePriceAdditions(id, price);
        this.summaryComponent.updateAdditions(this.selectedAdditions);
    }

    // show price for all additions
    calculatePriceAdditions(id: number, price: string) {
        if (this.selectedAdditions.indexOf(id) == -1 ) {
            this.selectedAdditions.push(id);
            this.additionBasket += parseInt(price);
        }
        else {
            this.selectedAdditions.splice(this.selectedAdditions.indexOf(id), 1);
            this.additionBasket -= parseInt(price);
        }
    }

    // previous page
    goBack() {
        if (this.configService.urlSettings.calculateFromUrl) {
            this.router.navigate(['/']);
        }
        else {
            this.location.back();
        }
    }

    // next step - confirmation
    goConfirmation() {
        this.router.navigate(['nf/confirmation']);
    }
}
