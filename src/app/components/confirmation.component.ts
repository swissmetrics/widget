import { Component, DoCheck, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AdditionsModel } from '../models/additions.model';
import { BookingModel, PhoneCodes } from '../models/booking.model';
import { CalculationPrices } from '../models/calculation.model';
import { RegulationsDialogComponent } from './dialog-regulations.component';
import { ReservationService } from '../services/reservation.service';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';
import { OtherService } from '../services/other.service';
import { PhonesService } from '../services/phones.service';

@Component({
    templateUrl: '../templates/confirmation.html',
    styleUrls: ['../style/additions.scss', '../style/confirmation.scss', '../style/summary.scss'],
    providers: [PhonesService]
})

export class ConfirmationComponent implements DoCheck, OnDestroy, OnInit {

    public userForm: FormGroup;
    public paymentMethod: string = 'online';
    public guestForm: string[] = [];
    public invoiceForm: string[] = [];
    public notesForm: string[] = [];
    public checkbox_panel_1: boolean = false;
    public checkbox_panel_2: boolean = false;
    public checkbox_panel_3: boolean = false;
    public showProgress: boolean = false;
    public pick: any;
    private promoCode: string;
    public subCode: Subscription;
    public disableBtn: boolean = true;
    public termsagree: boolean = false;
    public innerWidth: number;
    public showProgressSummary: boolean = true;
    public allPrice: {prices: CalculationPrices, currency: string};
    public selectedCountry: PhoneCodes = {short: 'pl', code: '+48', name: 'Polska'};
    private countryFound: boolean = true;

    constructor(
        private fb: FormBuilder,
        private dialog: MatDialog,
        private router: Router,
        private reservationService: ReservationService,
        private paramsService: ParamsService,
        private configService: ConfigService,
        private otherService: OtherService,
        private phonesService: PhonesService,
        private location: Location) {
            this.pick = this.paramsService.choosenRoom;
            if (!this.pick) {
                this.router.navigate(['/']);
            }
            this.createUserForm();
            this.subCode = this.otherService.getMessage().subscribe(rabat => {
                this.promoCode = rabat.code;
            });
    }

    ngOnInit() {
        this.phonesService.getPhones();
        this.innerWidth = window.innerWidth;
        for (let c in this.phonesService.countries) {
            if (this.phonesService.countries[c].short == this.configService.language.short) {
                this.selectedCountry = this.phonesService.countries[c];
                break;
            }
        }
    }

    // pass prices from child component
    onAllPrices(e) {
        this.allPrice = e;
    }

    // if show progress bar
    onShowProgress(e) {
        this.showProgressSummary = e;
    }

    createUserForm() {
        let emailInit = this.configService.urlSettings.email ? this.configService.urlSettings.email : '';
        this.userForm = this.fb.group({
            firstName: ['', Validators.required],
            lastName: ['', [Validators.required, Validators.minLength(3)]],
            email: [emailInit, [Validators.required, Validators.email]],
            phoneCountry: [this.selectedCountry.short],
            phone: [this.selectedCountry.code, [Validators.minLength(2), Validators.maxLength(20), Validators.pattern('(\\+)[0-9 ]+')]]
        });
    }

    // call booking api for one room
    bookOne() {

        this.showProgress = true;
        let params = this.paramsService.bookingUrl + this.readForm();

        this.reservationService
            .bookOne(params)
            .subscribe(ret => {
                this.showProgress = true;
                this.paramsService.bookingRet = ret;
                this.router.navigate([`nf/payment/${this.paymentMethod}`]);
            },
            error => {
                this.router.navigate(['/']);
            }
        );
    }

    // call api for several rooms
    bookMany(group: number) {

        this.showProgress = false;
        let params = this.paramsService.bookingUrl + this.readForm();

        this.reservationService
            .bookMany(params)
            .subscribe(ret => {
                this.showProgress = true;
                this.paramsService.bookingRet = ret;
                this.router.navigate([`nf/payment/${this.paymentMethod}`]);
            },
            error => {
                //this.router.navigate(['/']);
            }
        );
    }

    // previous page
    goBack() {
        if (this.configService.urlSettings.calculateFromUrl) {
            this.router.navigate(['nf/additions']);
        }
        else {
            this.location.back();
        }
    }

    // go to paymant page
    goPaymant() {
        if (this.pick.rooms.length == 1) {
            this.bookOne();
        } else if (this.pick.rooms.length > 1){
            this.bookMany(this.pick.rooms.length);
        }
    }

    // show modal window - regulations
    openRegulations() {
        this.dialog.open(RegulationsDialogComponent, {
            panelClass: 'regulations-modal',
        });
    }

    // select checkbox where any field is fill in - dane faktury
    panel_1_changed() {
        this.checkbox_panel_1 = false;
        for(let e in this.invoiceForm) {
            if (this.invoiceForm[e]) {
                this.checkbox_panel_1 = true;
                break;
            }
        }
    }

    // select checkbox where any field is fill in - dane gościa
    panel_2_changed() {
        this.checkbox_panel_2 = false;
        for(let e in this.guestForm) {
            if (this.guestForm[e]) {
                this.checkbox_panel_2 = true;
                break;
            }
        }
    }

    // select checkbox where any field is fill in - uwagi do rezerwacji
    panel_3_changed() {
        this.checkbox_panel_3 = false;
        for(let e in this.notesForm) {
            if (this.notesForm[e]) {
                this.checkbox_panel_3 = true;
                break;
            }
        }
    }

    // read data from form
    readForm() {
        let params = '&customer[c_firstname]=' + this.userForm.get('firstName').value;
        params += '&customer[c_lastname]=' + this.userForm.get('lastName').value;
        params += '&customer[c_email]=' + this.userForm.get('email').value;
        params += '&customer[language_id]=' + this.configService.language.id;
        params += this.notesForm['comment'] ? '&customer[c_notes]=' + this.notesForm['comment'] : '';

        let phone = this.userForm.get('phone').value;
        if (phone.length > 2) {
            params += '&customer[c_phone]=' + this.userForm.get('phone').value;
        }

        return params;
    }

    // detect changes in form
    ngDoCheck() {
        if (this.userForm.valid && this.termsagree) {
            this.disableBtn = false;
        } else {
            this.disableBtn = true;
        }

    }

    ngOnDestroy() {
        this.subCode.unsubscribe();
    }

    // select sountry code
    onChangeCountry() {
        this.selectedCountry  = this.userForm.get('phoneCountry').value;
        this.userForm.patchValue({phone: this.selectedCountry.code});
    }

    // phone typing
    onChangePhone(value: string) {
        if (value.length < 1) {
            this.userForm.patchValue({phone: '+'});
            this.countryFound = false;
        }
        if (value.length > 4 || value.length < 2) {
            return;
        }
        for (let c of this.phonesService.availablePhones) {
            if (c.code === value) {
                this.selectedCountry = c;
                this.countryFound = true;
                break;
            }
        }
        if (!this.countryFound) {
            this.selectedCountry = {short: '', code: '', name: ''};
        }
    }

}
