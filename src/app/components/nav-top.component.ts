import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';
import { UrlParamsService } from '../services/urlparams.service';
import { ParamsModel } from '../models/params.model';
import { PeopleModel } from '../models/people.model';
import { PreferencesModel } from '../models/preferences.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { NavTopPeopleComponent } from './nav-top-people.component'

import { FormControl } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import * as moment from 'moment';

// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'dddd, DD MMM YYYY',
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    },
};

@Component({
    selector: 'nav-top',
    templateUrl: '../templates/nav-top.html',
    styleUrls: ['../style/nav-top.scss'],
    providers: [DatePipe,
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
    ]
})

export class NavtopComponent implements OnInit, OnDestroy {

    @Input() hideBottomMenu: boolean;

    public dateFrom: Date;
    public dateTo: Date;
    public minDateFrom: Date = new Date();
    public minDateTo: Date;
    public maxDateFrom: Date | null = null;;
    public maxDateTo: Date | null = null;
    public params: ParamsModel;
    public daysStay: number;
    public rooms: number = 0;
    public adults: number = 1;
    public kids: number = 0;
    public allPeople = [];
    public lang: string;
    public showMobileMenu: boolean = false;
    public showMobileLngMenu: boolean = false;
    public date = new FormControl(moment());
    public subPreferences: Subscription;
    public preferences: PreferencesModel;
    private routeSub: any;
    private routeSubQ: any;

    constructor(
        private paramsService: ParamsService,
        public configService: ConfigService,
        public urlParamsService: UrlParamsService,
        private router: Router,
        private route: ActivatedRoute,
        private datePipe: DatePipe) {

            this.init();

            this.routeSubQ = this.route.queryParams.subscribe((params: Params) => {
                if (Object.keys(params).length) {
                    this.subscribeGetParams(params);
                }
            });

            this.routeSub = this.route.params.subscribe((params: Params) => {
                if (Object.keys(params).length) {
                    this.subscribeRouteParams(params);
                }
                else {
                    this.searchExist();
                }
            });

            this.preferences = this.configService.preferences;
            this.subPreferences = this.configService.pullPreferences().subscribe(preferences => {
                this.preferences = preferences;
            });
    }

    // at the begining
    init() {
        this.defaultDates();
        this.calculateNights();
    }

    // if params are set
    searchExist() {
        if (this.configService.urlSettings.calculateFromUrl) {
            this.loadingGetDates();
            this.calculateNights();
        }
    }

    subscribeGetParams(params) {
        this.urlParamsService.setParams = params;
        this.urlParamsService.init();
        this.loadingGetDates();
        this.calculateNights();
    }

    subscribeRouteParams(params) {
        let adult: number[] = [1];
        let kidsAge = [];
        let allPeople = [];
        let allAdults = 1;
        let allKids = 0;
        let rooms = 1;

        if(params['people']) {
            let roomsAmount = params['people'].split('$');
            rooms = roomsAmount.length;
            allAdults = 0;
            for(let r in roomsAmount) {
                let kidsCount = 0;
                let adults = roomsAmount[r].split(':');
                adult[r] = adults[0];
                allAdults += parseInt(adult[r]);
                kidsAge[r] = adults[1] ? adults[1].split(',') : [];
                kidsCount = kidsAge[r].length
                allKids += kidsCount;
                allPeople.push({adults: adult[r], kids: kidsCount, age: kidsAge[r]});
            }
        }

        // set params
        this.paramsService['searchParams'] = {
            from: params['from'],
            to: params['to'],
            rooms: rooms,
            adults: allAdults,
            kids: allKids,
            people: allPeople
        }

        this.defaultDates();

        if (params['from']) {
            this.dateFrom = params['from'];
            this.dateTo = params['to'];
        }

        this.calculateNights();
    }

    ngOnInit() {
        let savedLanguage = this.configService.getLanguage();
        this.lang = savedLanguage ? savedLanguage[0] : this.configService.defaultLanguage;
    }

    // set dateFrom on today, dateTo + 1 day
    defaultDates() {
        this.dateFrom = new Date();
        this.dateTo = new Date(this.dateFrom);
        this.dateTo.setDate(this.dateTo.getDate() + 1);
        this.setHouers();
        this.setMinDate();
        this.setMaxDate();
    }

    // set dates from loading script
    loadingGetDates() {

        if (this.configService.urlSettings.dateFrom) {
            this.dateFrom = new Date(this.configService.urlSettings.dateFrom);
        }
        if (this.configService.urlSettings.dateTo) {
            this.dateTo = new Date(this.configService.urlSettings.dateTo);
        }
        this.setHouers();
    }

    setHouers() {
        this.dateFrom.setHours(0, 0, 0, 0);
        this.dateTo.setHours(0, 0, 0, 0);
    }

    calculateNights() {
        let dF = moment(this.dateFrom).unix();
        let dT = moment(this.dateTo).unix();
        this.daysStay = Math.floor((dT - dF) / (60 * 60 * 24));
        // if max stay in the config is set
        if (this.configService.config.max_stay) {
            if (this.daysStay > this.configService.config.max_stay) {
                let newToDate = moment(this.dateFrom).add(this.configService.config.max_stay, 'days');
                this.dateTo = new Date(newToDate.format());
                this.daysStay = this.configService.config.max_stay;
            }
        }
        this.paramsService.daysStay = this.daysStay;
    }

    // dateTo must be smaller than dateFrom
    setMinDate() {
        this.minDateTo = new Date(moment(this.dateFrom).unix()*1000);
        this.minDateTo.setDate(moment(this.dateFrom).date() + 1);
        if (this.dateTo <= this.dateFrom) {
            this.dateTo = this.minDateTo;
        }
    }

    // max date is limited by api
    setMaxDate() {
        if (this.configService.config.max_reservation_date) {
            this.maxDateTo = new Date(this.configService.config.max_reservation_date);
            this.maxDateFrom = new Date(this.configService.config.max_reservation_date);
            this.maxDateFrom.setDate(this.maxDateFrom.getDate() - 1);
        }
    }

    // changed date in datapicker 'From'
    changeDateFrom(event: MatDatepickerInputEvent<Date>) {
        this.setMinDate();
        this.calculateNights();
    }

    // changed date in datapicker 'To'
    changeDateTo(event: MatDatepickerInputEvent<Date>) {
        this.calculateNights();
    }

    // detect changing number of people
    onPeopleChange(e: {rooms: number, adults: number, kids: number, all: any}) {
        this.rooms = e.rooms;
        this.adults = e.adults;
        this.kids = e.kids;
        this.allPeople = [];
        for(let a of e.all) {
            let kidAge = [];
            for(let k = 0; k < a.kids; k++) {
                kidAge.push(a['age_'+k]);
            }
            this.allPeople.push({adults: a.adults, kids: a.kids, age: kidAge});
        }
    }

    // detect changing language
    onLanguageChange(event: {lng: string, id: string}) {
        let savedLanguage = this.configService.getLanguage();
        if (this.lang != event.lng) {
            let lngStr = event.lng + '|' + event.id;
            this.configService.setLanguage(lngStr);
            location.reload()
        }
    }

    // search rooms - click on "sprawdź dostępność" button
    search() {
        this.configService.urlSettings.calculateFromUrl = false;
        this.paramsService['searchParams'] = {
            from: this.dateFrom,
            to: this.dateTo,
            rooms: this.rooms,
            adults: this.adults,
            kids: this.kids,
            people: this.allPeople
        }

        let fromT = this.datePipe.transform(this.dateFrom, 'yyyy-MM-dd');
        let toT = this.datePipe.transform(this.dateTo, 'yyyy-MM-dd');
        let urlPeople = this.paramsService.makePeopleUrl(this.allPeople);

        this.router.navigate([`nf/rooms/${fromT}/${toT}`, {people: urlPeople || 1}]);

    }

    ngOnDestroy() {
        this.subPreferences.unsubscribe();
        this.routeSub.unsubscribe();
        this.routeSubQ.unsubscribe();
    }

}
