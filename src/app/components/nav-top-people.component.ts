import { Component, Output, EventEmitter, ElementRef } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';

@Component({
    selector: 'menu-people',
    templateUrl: '../templates/nav-top-people.html'
})

export class NavTopPeopleComponent {

    @Output() noAdultsKids = new EventEmitter();

    public peopleForm: FormGroup;
    public rooms: number = 0;
    public showNextRoom: boolean = true;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private paramsService: ParamsService,
        private configService: ConfigService,
        private el: ElementRef) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.rooms = 0;
            this.peopleForm = this.formBuilder.group({
                peopleRow: this.formBuilder.array([])
            });
            for (let r = 0; r < this.paramsService['searchParams'].rooms; r++) {
                this.addPeople(r);
            }
        });
    }

    // init people form
    initPeople(order: number = 0) {

        let people = this.paramsService['searchParams'].people;
        //let pCount = {};

        let age_number = {};
        for (let i = 0; i <= this.configService.config.max_children_count; i++) {
            age_number['age_'+i] = [5];
        }

        let pCount = Object.assign({
            adults: [1],
            kids: [0]
        }, age_number);


        if (people.length > 0) {
            age_number = {};
            for (let i = 0; i <= this.configService.config.max_children_count; i++) {
                age_number['age_'+i] = [people[order].age[0] ? +people[order].age[0] : 5];
            }
            pCount = Object.assign({
                adults: [+people[order].adults],
                kids: [people[order].kids]
            }, age_number);
        }

        return this.formBuilder.group(pCount);
    }

    // add new row of adults, kids
    addPeople(order: number = 0) {
        const control = <FormArray>this.peopleForm.controls['peopleRow'];
        const addrCtrl = this.initPeople(order);
        control.push(addrCtrl);
        this.rooms++;
        this.calculatePeople();
        setTimeout(() => {
            this.el.nativeElement.querySelector('.people-row-wrapper').scrollTop = 444;
        }, 0);
        if (this.rooms >= this.configService.config.max_room_count) {
            this.showNextRoom = false;
        }
    }

    // remove people row
    removePeople(i: number) {
        const control = <FormArray>this.peopleForm.controls['peopleRow'];
        control.removeAt(i);
        this.rooms--;
        this.showNextRoom = true;
        this.calculatePeople();
    }

    // show choosen number of guests
    calculatePeople() {
        let adults = 0;
        let kids = 0;
        for(let row of this.peopleForm.value.peopleRow) {
            adults += row.adults;
            kids += row.kids;
        }

        this.emitOnChange({rooms: this.rooms, adults: adults, kids: kids, all: this.peopleForm.value.peopleRow});
    }

    // emit event to parent component
    emitOnChange(e) {
        this.noAdultsKids.emit(e);
    }

    // listening for changes from nav-top-people-row
    onChangedPeople() {
        this.calculatePeople();
    }

}
