import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ParamsService } from '../services/params.service';
import { ConfigService } from '../services/config.service';
import { ReservationService } from '../services/reservation.service';
import { CalculationPrices } from '../models/calculation.model';
import { BookingModel, PaymentMethod } from '../models/booking.model';

@Component({
    templateUrl: '../templates/payment-online-bank.html',
    styleUrls: ['../style/payments.scss']
})

export class PaymentBankComponent implements OnInit {

    public paymentKind: number = 2;
    public prices: CalculationPrices;
    public currency: string;
    public allowedPayment: number;
    public bankIcon: any;
    public response: BookingModel;
    public paymentMethod: PaymentMethod;

    constructor(
        private route: ActivatedRoute,
        private paramsService: ParamsService,
        private configService: ConfigService,
        private reservationService: ReservationService,
        private sanitizer: DomSanitizer,
        private router: Router) {
            this.prices = this.paramsService.bookingPrices;
            this.currency = this.configService.config.system_currency;
            this.allowedPayment = this.configService.config.payment_type;
            this.response = this.paramsService.bookingRet;
            if (!this.prices || !this.response) {
                this.router.navigate([`/`]);
            }
            if (this.allowedPayment == 3) {
                this.paymentKind = 3;
            }
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            for (let pay of this.response.payment_methods) {
                if (pay.id == params['bank']) {
                    this.bankIcon = this.getIcon(pay.icon);
                    this.paymentMethod = pay;
                    break;
                }
            }
        });
    }

    // go to choosing banks
    goBack() {
        this.router.navigate([`nf/payment/online`]);
    }

    // return url to icon
    getIcon(img: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${img})`);
    }

    // send to dotpay
    pay() {
        let params = `method_id=${this.paymentMethod.id}&reservation_id=${this.response.id}&`;
        params += `payment_type=${this.paymentKind}&payment_token=${this.response.payment_token}`;
        this.reservationService
            .paymentLink(params)
            .subscribe(ret => {
                parent.location.href = ret;
            },
            error => {
                this.router.navigate(['/']);
            }
        );
    }
}
