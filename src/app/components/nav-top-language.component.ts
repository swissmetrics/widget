import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConfigService } from '../services/config.service';

@Component({
    selector: 'menu-language',
    templateUrl: '../templates/nav-top-language.html'
})

export class NavTopLanguageComponent {

    @Output() pickLanguage = new EventEmitter();

    constructor(public configService: ConfigService) {
    }

    // emit event to parent component
    setLanguage(lng: string, id: string) {
        this.pickLanguage.emit({lng: lng, id: id});
    }

}
