import { Component, Input, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { OtherService } from '../services/other.service';
import { ConfigService } from '../services/config.service';
import { PreferencesModel } from '../models/preferences.model';

@Component({
    selector: 'menu-rabat',
    templateUrl: '../templates/nav-top-rabat.html'
})

export class NavTopRabatComponent implements OnDestroy {

    public code: string;
    public subCode: Subscription;
    public subPreferences: Subscription;
    public preferences: PreferencesModel;

    constructor(private otherService: OtherService, private configService: ConfigService) {
        this.code = this.otherService.rabatCode;
        this.subCode = this.otherService.getMessage().subscribe(rabat => {
            this.code = rabat.code;
        });
        this.preferences = this.configService.preferences;
        this.subPreferences = this.configService.pullPreferences().subscribe(preferences => {
            this.preferences = preferences;
        });
    }

    addCode() {
        this.otherService.rabatCode = this.code;
        this.otherService.sendMessage(this.code);
        if (this.code) {
            this.otherService
                .checkPromoCode(`promo_code=${this.code}`)
                .subscribe(
                    ret => {
                        this.otherService.rabatDetails = ret;
                    },
                    error => {
                        //this.otherService.rabatCode = null;
                        this.otherService.rabatDetails = null;
                    }
                );
        }
    }

    ngOnDestroy() {
        this.subCode.unsubscribe();
        this.subPreferences.unsubscribe();
    }
}
