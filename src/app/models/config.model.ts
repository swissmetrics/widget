export interface ConfigModel {
    allow_reservations: boolean,
    arrival_time: string,
    custom_css_url: string,
    departure_time: string,
    home_link: string,
    hotel_phone: string,
    languages: {
        id: number,
        lg_label: string,
        lg_code: string
    }[],
    max_reservation_date: string,
    max_stay: number,
    payment_type: number,
    promotions_enabled: boolean,
    show_basic_payments: boolean,
    show_online_payments: boolean,
    show_room_max_persons: boolean,
    stay_forward: number,
    system_currency: string,
    base_url: string,
    max_adult_count: number,
    max_children_count: number,
    max_room_count: number
}

export interface UrlSettingsModel {
    dateFrom: string,
    dateTo: string,
    adultCount: number,
    childrenCount: number,
    childrenAges: number[],
    package: number,
    room: number,
    configId: number,
    equipment: number[],
    calculateFromUrl: boolean,
    email?: string
}
