export interface MealsModel {
    id: number,
    name: string,
    in_price: boolean
}
