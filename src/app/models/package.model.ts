export interface PackageModel {
    id: number,
    p_label: string,
    days: number,
    days_type: number,
    allowed_arrival_week_days: number[],
    meals: number[],
    additions: number[],
    photos: string[],
    title: string,
    description: string
}

export interface PackageSearchParams {
    rooms: number,
    from: string,
    to: string,
    adult: number[],
    age: any
}
