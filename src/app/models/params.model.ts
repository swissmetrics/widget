export interface ParamsModel {
    id: number,
    slug: string,
    from_date?: string,
    to_date?: boolean,
    lng?: string,
    package_id?: number,
    offer_id?: number,
    force_search?: boolean,
    force_packages?: boolean
}
