export interface RegulationsModel {
    [key: string]: string
}

// promo code
export interface PromoModel {
    id: number,
    vp_discount_value: number,
    vp_discount_type: string,
    vp_apply_discount_to: string[]
}
