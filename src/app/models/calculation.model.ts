export interface CalculationPrices  {
    stay_price: number,
    installment: number,
    additions_price: number,
    r_grand_total: number
};

export interface CalculationModel {
    prices_before_promotion: CalculationPrices;
    prices: CalculationPrices;
}

interface CalculationStay  {
    [key: string]: {
      stay_price: number,
      additions: {
          id: number,
          a_price: number,
          a_label: string
      }[]
    }
};

export interface CalculationExtModel {
      prices: CalculationStay;
      prices_before_promotion: CalculationStay;
}
