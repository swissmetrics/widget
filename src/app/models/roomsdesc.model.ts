export interface RoomsDescModel {
    equipment: {
        id: string,
        type: string,
        value: string
    }[],
    roomstandard_id: string,
    rsl_description: string,
    rsl_dopelniacz: string,
    rsl_name: string,
}
