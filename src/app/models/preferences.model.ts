export interface PreferencesModel {
    hotel_name: string,
    header_text: string,
    price_for: string,
    no_free_rooms: string,
    promo_additional_info: string,
    welcome_icons: {
        text: string,
        ligature: string
    } [],
    filters: {
        equipment: {
              id: number,
              eq_label: string,
              eq_type: string,
              eq_icon: string,
              eq_format: number
        } []
    }
}
