export interface AdditionsModel {
    additions: {
        id: number,
        a_label: string,
        a_price: number,
        group_id: number
    }[],
    groups: {
        id: number,
        ag_label: string,
        category_id: number
    }[],
    categories: {
        id: number,
        ac_label: string
    }[]
}
