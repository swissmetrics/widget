export interface BookingModel {
    id: string,
    payment_data: string,
    payment_methods: PaymentMethod[],
    payment_token: string
}

export interface PaymentMethod {
    id: number,
    name: string,
    icon: string
}

export interface PhoneCodes {
    short: string,
    code: string,
    name: string
}
