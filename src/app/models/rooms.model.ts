export interface RoomsModel {
    id: number,
    available: number,
    price: {
      config_id: {
          available: number,
          price: number,
          price_before_promotion: number,
          rooms: {
              id: number,
              ro_number: string
          }[]
      }[]
    }
}
