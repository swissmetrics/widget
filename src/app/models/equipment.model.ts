export interface EquipmentModel {
   id: number,
   eq_label: string,
   eq_type: 'tekstowe' | 'checkbox' | 'textarea' | 'liczbowe' | 'zmiennoprzecinkowe',
   eq_icon: string,
   eq_format: number
}
