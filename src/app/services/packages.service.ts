import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { PackageModel } from '../models/package.model';

@Injectable()
export class PackagesService {

    constructor(private restService: RestService) {
    }

    // /package_list
    packages(body: string): Observable<PackageModel[]> {
        return this.restService.post('package_list', body, 'application/x-www-form-urlencoded');
    }
}
