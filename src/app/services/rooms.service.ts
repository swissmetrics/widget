import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { RoomsModel } from '../models/rooms.model';
import { RoomsDescModel } from '../models/roomsdesc.model';
import { EquipmentModel } from '../models/equipment.model';
import { MealsModel } from '../models/meals.model';

@Injectable()
export class RoomsService {

    constructor(private restService: RestService) {
    }

    // /room_description
    roomDescription(body: string): Observable<RoomsDescModel[]> {
        return this.restService.post('rooms_description', body, 'application/x-www-form-urlencoded');
    }

    // /room_availability
    roomAvailability(body: string): Observable<RoomsModel[]> {
        return this.restService.post('room_availability', body, 'application/x-www-form-urlencoded');
    }

    // /rooms_availability
    roomsAvailability(body: string): Observable<any> {
        return this.restService.post('rooms_availability', body, 'application/x-www-form-urlencoded');
    }

    // /room_package_availability
    roomPackageAvailability(body: string): Observable<RoomsModel[]> {
        return this.restService.post('room_package_availability', body, 'application/x-www-form-urlencoded');
    }

    // /rooms_package_availability
    roomsPackageAvailability(body: string): Observable<any> {
        return this.restService.post('rooms_package_availability', body, 'application/x-www-form-urlencoded');
    }

    // /equipment
    equipment(body: string): Observable<EquipmentModel[]> {
        return this.restService.post('equipment', body, 'application/x-www-form-urlencoded');
    }

    // /meals
    meals(body: string): Observable<MealsModel[]> {
        return this.restService.post('meals', body, 'application/x-www-form-urlencoded');
    }

    // /last reservation
    lastBooked(): Observable<number> {
        return this.restService.post('last_booked', '', 'application/x-www-form-urlencoded');
    }
}
