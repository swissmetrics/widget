import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { RestService } from './rest.service';
import { ConfigModel, UrlSettingsModel } from '../models/config.model';
import { PreferencesModel } from '../models/preferences.model';

@Injectable()
export class ConfigService {

    public config: ConfigModel;
    public urlSettings: UrlSettingsModel;
    public preferences: PreferencesModel;
    public defaultLanguage: string = 'pl';
    public language: {short: string, id: number} = {
        short: 'pl',
        id: 1
    };
    public rabatCode: string;
    private subjectPreferences = new Subject<any>();

    constructor(private restService?: RestService) {
        this.config = {
            allow_reservations: true,
            arrival_time: "14:00",
            custom_css_url: "",
            departure_time: "12:00",
            home_link: "",
            hotel_phone: "",
            languages: [{
                id: 1,
                lg_label: "Polski",
                lg_code: "pl"
            }],
            max_reservation_date: "2018-09-30",
            max_stay: 31,
            payment_type: 1,
            promotions_enabled: false,
            show_basic_payments: true,
            show_online_payments: true,
            show_room_max_persons: false,
            stay_forward: 12,
            system_currency: "PLN",
            base_url: "http://37.187.198.34/nf-hotels/",
            max_adult_count: 3,
            max_children_count: 14,
            max_room_count: 5
        };
        this.urlSettings = {
            calculateFromUrl: false,
            dateFrom: '',
            dateTo: '',
            adultCount: 1,
            childrenCount: 0,
            childrenAges: [],
            package: null,
            room: null,
            configId: null,
            equipment: []
        };
    }

    // get config by API
    getConfig(service: string = ''): Observable<ConfigModel> {
        return this.restService.get(service);
    }

    // get preferences by API
    getPreferences(body: string): Observable<PreferencesModel> {
        return this.restService.post('preferences', body, 'application/x-www-form-urlencoded');
    }

    // share preferences between components
    sharePreferences(response: PreferencesModel) {
        this.subjectPreferences.next(response);
    }

    // get preferences form Observable
    pullPreferences(): Observable<any> {
        return this.subjectPreferences.asObservable();
    }

    // return image
    // 'w1200-h1200',
    // 'w140-c',
    // 'w200-c',
    // 'w245-h160-c',
    // 'w400-c',
    // 'w500-c',
    // 'h120-c',
    // 'h60-c',
    // 'w900-h550-c',
    getPhoto(name: string, size: string) {
        return `http://api.nfhotel.pl/imagefly/${size}/${name}`;
    }

    // make thousend separator
    formatPrice(price: string) {
        let ret = price.replace('ABC', '');
        return ret.replace(',', ' ');
    }

    getLanguage(): string[] {
        let lngStr = localStorage.getItem('nfWidgetLng');
        return lngStr ? lngStr.split('|') : null;
    }

    setLanguage(lng: string) {
        localStorage.setItem('nfWidgetLng', lng);
        sessionStorage.setItem('nfChangeLng', '1');
    }

}
