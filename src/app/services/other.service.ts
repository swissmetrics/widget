import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { RestService } from './rest.service';
import { RegulationsModel, PromoModel } from '../models/other.model';

@Injectable()
export class OtherService {

    public rabatCode: string;
    public rabatDetails: PromoModel | boolean;
    private subject = new Subject<any>();

    constructor(private restService: RestService) {
    }

    // /regulations
    getRegulations(body: string): Observable<RegulationsModel> {
        return this.restService.post('regulations', body, 'application/x-www-form-urlencoded');
    }

    // /check_promo_code
    checkPromoCode(body: string): Observable<PromoModel> {
        return this.restService.post('check_promo_code', body, 'application/x-www-form-urlencoded');
    }

    // send promocode to other components
    sendMessage(message: string) {
        this.subject.next({ code: message });
    }

    clearMessage() {
        this.subject.next();
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
