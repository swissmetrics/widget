import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { CalculationModel, CalculationExtModel } from '../models/calculation.model';
import { BookingModel } from '../models/booking.model';

@Injectable()
export class ReservationService {

    constructor(private restService: RestService) {
    }

    // /calculate_one
    calculateOne(body: string): Observable<CalculationModel> {
        return this.restService.post('calculate_one', body, 'application/x-www-form-urlencoded');
    }

    // /calculate_one
    calculateMany(body: string): Observable<CalculationModel[]> {
        return this.restService.post('calculate_many', body, 'application/x-www-form-urlencoded');
    }

    // /calculate_one_extended
    calculateExtOne(body: string): Observable<CalculationExtModel> {
        return this.restService.post('calculate_one_extended', body, 'application/x-www-form-urlencoded');
    }

    // /calculate_many_extended
    calculateExtMany(body: string): Observable<CalculationExtModel> {
        return this.restService.post('calculate_many_extended', body, 'application/x-www-form-urlencoded');
    }

    // /book_one
    bookOne(body: string): Observable<BookingModel> {
        return this.restService.post('book_one', body, 'application/x-www-form-urlencoded');
    }

    // /calculate_many_extended
    bookMany(body: string): Observable<BookingModel> {
        return this.restService.post('book_many', body, 'application/x-www-form-urlencoded');
    }

    // /get_payment_link
    paymentLink(body: string): Observable<string> {
        return this.restService.post('get_payment_link', body, 'application/x-www-form-urlencoded');
    }
}
