import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { PhoneCodes } from '../models/booking.model';

@Injectable()
export class PhonesService {

    public countries: PhoneCodes[] = [
        {short: 'pl', code: '+48', name: 'Polska'},
        {short: 'de', code: '+49', name: 'Niemcy'},
        {short: 'ru', code: '+7', name: 'Rosja'},
        {short: 'en', code: '+44', name: 'GB'},
        {short: 'at', code: '+43', name: 'Austria'},
        {short: 'by', code: '+375', name: 'Białoruś'},
        {short: 'be', code: '+32', name: 'Belgia'},
        {short: 'bg', code: '+358', name: 'Bułgaria'},
        {short: 'cz', code: '+420', name: 'Czechy'},
        {short: 'dk', code: '+45', name: 'Dania'},
        {short: 'fi', code: '+358', name: 'Finlandia'},
        {short: 'fr', code: '+33', name: 'Francja'},
        {short: 'gr', code: '+30', name: 'Grecja'},
        {short: 'hu', code: '+36', name: 'Węgry'},
        {short: 'is', code: '+354', name: 'Islandia'},
        {short: 'ie', code: '+358', name: 'Irlandia'},
        {short: 'it', code: '+39', name: 'Włochy'},
        {short: 'lv', code: '+371', name: 'Łotwa'},
        {short: 'lt', code: '+370', name: 'Litwa'},
        {short: 'nl', code: '+31', name: 'Holandia'},
        {short: 'no', code: '+47', name: 'Norwegia'},
        {short: 'pt', code: '+351', name: 'Portugalia'},
        {short: 'ro', code: '+40', name: 'Rumunia'},
        {short: 'sk', code: '+421', name: 'Słowacja'},
        {short: 'se', code: '+46', name: 'Szwecja'},
        {short: 'ch', code: '+41', name: 'Szwajcaria'},
        {short: 'ua', code: '+380', name: 'Ukraina'},
        {short: 'es', code: '+34', name: 'Hiszpania'},
        {short: 'tr', code: '+90', name: 'Turcja'},
        {short: 'il', code: '+972', name: 'Izrael'},
        {short: 'us', code: '+1', name: 'USA'},
        {short: 'ca', code: '+1', name: 'Kanada'},
        {short: 'cu', code: '+53', name: 'Kuba'},
        {short: 'ar', code: '+54', name: 'Argentyna'},
        {short: 'br', code: '+55', name: 'Brazylia'},
        {short: 'cl', code: '+56', name: 'Chile'},
        {short: 'mx', code: '+52', name: 'Meksyk'},
        {short: 'au', code: '+61', name: 'Australia'},
        {short: 'nz', code: '+64', name: 'Nowa Zelandia'},
        {short: 'cn', code: '+86', name: 'Chiny'},
        {short: 'jp', code: '+81', name: 'Japonia'},
        {short: 'sa', code: '+966', name: 'Arabia Saudyjska'},
        {short: 'in', code: '+91', name: 'Indie'},
        {short: 'kz', code: '+7', name: 'Kazachstan'},
        {short: 'za', code: '+27', name: 'RPA'},
        {short: 'ma', code: '+212', name: 'Maroko'},
        {short: 'ke', code: '+254', name: 'Kenia'},
        {short: 'eg', code: '+20', name: 'Egipt'}

    ];

    public availablePhones: PhoneCodes[];
    private firstCodes: PhoneCodes[] = [];

    constructor (
        private configService: ConfigService
    ) {}

    getPhones() {
        this.countries.sort((a, b): number => {
            return (a.short > b.short ? 1 : -1);
        });

        for (let l of this.configService.config.languages) {
            for (let c in this.countries) {
                if (l.lg_code == this.countries[c].short) {
                    this.firstCodes.push(this.countries[c]);
                    delete this.countries[+c];
                    break;
                }
            }
        }

        this.availablePhones = this.firstCodes.concat( this.countries.filter(Boolean) );
    }
}
