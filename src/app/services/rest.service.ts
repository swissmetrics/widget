import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ParamsService } from './params.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class RestService {

    server: string = `http://booking.nfhotel.pl/api2/`;

    constructor(private http: HttpClient, private paramsService: ParamsService) {
        this.server += `${paramsService.widgetParams.slug}/${paramsService.widgetParams.id}/`;
    }

    // GET method - API
    get(service: string): Observable<any> {
        return this.http
            .get(`${this.server}${service}`)
            .catch((err: HttpErrorResponse) => err.error.message || 'Server error');
    }

    // POST method
    post(service: string, body: string, type: string = 'application/json'): Observable<any> {

        const httpOptions = {
            headers: new HttpHeaders({
               'Content-Type': type
            })
        };

        return this.http
            .post(`${this.server}${service}`, body, httpOptions)
            .catch((err: HttpErrorResponse) => Observable.throw(err.error || 'Server error'));

    }

}
