import { Injectable } from '@angular/core';
import { Router, Params } from '@angular/router';
import { ParamsService } from './params.service';
import { ConfigService } from './config.service';
import { RoomsService } from '../services/rooms.service';

@Injectable()
export class UrlParamsService {

    private roomDescTr: object = {};
    private params: Params;

    constructor (
        private paramsService: ParamsService,
        private configService: ConfigService,
        private roomsService: RoomsService,
        private router: Router,
    ) {}

    set setParams(params: any) {
        this.params = params;
    }

    init() {
        this.configService.urlSettings.calculateFromUrl = true;
        let paramsKeys = Object.keys(this.params);
        if (paramsKeys.length) {
            if (paramsKeys.indexOf('date_from') !== -1) {
                this.configService.urlSettings.dateFrom = this.params.date_from;
                this.isDateFrom();
            }
            if (paramsKeys.indexOf('date_to') !== -1) {
                this.configService.urlSettings.dateTo = this.params.date_to;
                this.isDateTo();
            }
            if (paramsKeys.indexOf('room') !== -1) {
                this.configService.urlSettings.room = this.params.room;
                this.isRoom();
            }
            if (paramsKeys.indexOf('config_id') !== -1) {
                this.configService.urlSettings.configId = this.params.config_id;
            }
            if (paramsKeys.indexOf('package') !== -1) {
                this.configService.urlSettings.package = this.params.package;
            }
            if (paramsKeys.indexOf('adult_count') !== -1) {
                this.configService.urlSettings.adultCount = this.params.adult_count;
                this.isAdult();
            }
            if (paramsKeys.indexOf('children_ages') !== -1) {
                let children = this.params.children_ages.split(',');
                children.forEach(function (item, index) {
                    if (item <= 0 || item > 16) {
                        children.splice(index, 1);
                    }
                });
                this.configService.urlSettings.childrenCount = children.length;
                this.configService.urlSettings.childrenAges = children;
                this.isChildren(children);
            }
            if (paramsKeys.indexOf('equipment') !== -1) {
                let equipment = this.params.equipment.split(',');
                this.configService.urlSettings.equipment = equipment;
                this.paramsService.choosenAddictions = equipment;
            }
            if (paramsKeys.indexOf('email') !== -1) {
                this.configService.urlSettings.email = this.params.email;
            }
            this.moveOn();
        }
    }

    // if is set &date_from
    isDateFrom() {
        let dateFrom = new Date(this.params.date_from);
        this.paramsService['searchParams']['from'] = dateFrom;
    }

    // if is set &date_to
    isDateTo() {
        let dateTo = new Date(this.params.date_from);
        this.paramsService['searchParams'].to = dateTo;
    }

    // if is set &room
    isRoom() {
        this.paramsService['searchParams'].rooms = 1;
    }

    // if is set &adult_count
    isAdult() {
        this.paramsService['searchParams'].adults = this.params.adult_count;
        this.paramsService['searchParams'].people = [{adults: this.params.adult_count, kids: 0, age: []}];
    }

    // if is set &children_ages
    isChildren(ages: number[]) {
        let kids = ages.length;
        this.paramsService['searchParams'].kids = kids;
        this.paramsService['searchParams'].people = [{adults: this.params.adult_count, kids: kids, age: ages}];
    }

    setChoosenRoom(roomDesc) {
        let event = {
            room: {id: this.params.room},
            desc: roomDesc
        };
        this.paramsService.choosenRoom = {
            rooms: [event],
            search: this.paramsService['searchParams'],
            package: null
        };
    }

    moveOn() {
        if (this.params.date_from && this.params.date_to && this.params.adult_count) {
            if (this.params.room && this.params.config_id) {
                this.roomDescription();
            }
            else if (this.params.package) {
                this.goPackage();
            }
            else {
                this.goSearch();
            }
        }
    }

    // description of all rooms
    roomDescription() {
        let roomDesc = {};
        this.roomsService
            .roomDescription(`date_from=${this.params.date_from}&date_to=${this.params.date_to}&adult_count=1`)
            .subscribe(ret => {
                for(let r of ret) {
                    if (r.roomstandard_id == this.params.room) {
                        roomDesc = r
                        break;
                    }
                }
                this.setChoosenRoom(roomDesc);
                this.goConfirmation();
            });
    }

    // redirect if can be booked room
    goConfirmation() {
        this.router.navigate(['nf/confirmation']);
    }

    // redirect to package
    goPackage() {
        let urlPeople = this.paramsService.makePeopleUrl( this.paramsService['searchParams'].people );
        this.router.navigate([
            `nf/rooms/${this.params.date_from}/${this.params.date_to}`,
            {people: urlPeople || 1, package: this.params.package}
        ]);
    }

    // redirect to rooms search
    goSearch() {
        let urlPeople = this.paramsService.makePeopleUrl( this.paramsService['searchParams'].people );
        this.router.navigate([`nf/rooms/${this.params.date_from}/${this.params.date_to}`, {people: urlPeople || 1}]);
    }
}
