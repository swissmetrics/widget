import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { AdditionsModel } from '../models/additions.model';

@Injectable()
export class AdditionsService {

    constructor(private restService: RestService) {
    }

    // /additions
    getAdditions(body: string): Observable<AdditionsModel[]> {
        return this.restService.post('additions', body, 'application/x-www-form-urlencoded');
    }
}
