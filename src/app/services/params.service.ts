import { Injectable } from '@angular/core';
import { ParamsModel } from '../models/params.model';
import { BookingModel } from '../models/booking.model';
import { CalculationPrices } from '../models/calculation.model';

@Injectable()
export class ParamsService {

    public widgetParams: ParamsModel;
    public searchParams: {
        from: Date,
        to: Date,
        rooms: number,
        adults: number,
        kids: number,
        people: { adults: number, kids: number, age: number[] }[]
    };
    public choosenRoom: any;
    public choosenAddictions: number[] = [];
    public bookingUrl: string;
    public bookingRet: BookingModel;
    public bookingPrices: CalculationPrices;
    public daysStay: number;

    constructor() {
        this.getParams();
        this.searchParams = {
            from: new Date(),
            to: new Date(),
            rooms: 1,
            adults: 1,
            kids: 0,
            people: [{adults: 1, kids: 0, age: []}]
        };
    }

    // read widget params
    getParams() {
        this.widgetParams = {
            id: 40,
            slug: 'demo'
        };
    }

    // save choosen room in the session storage
    saveRoom(choosenRoom, searchParams) {
        let obj = {
            room: choosenRoom,
            params: searchParams
        };
        sessionStorage.setItem('nfChoosenRoom', JSON.stringify(obj));
    }

    // build url for kids age
    makeChildrenUrl(kids: number[], group: number | boolean = false) {
        let url = '';
        if (kids.length) {
            url = (group !== false) ? `&rooms[${group}][children_count]=${kids.length}` : `&children_count=${kids.length}`;
            for(let i in kids) {
                url += (group !== false) ? `&rooms[${group}][children_ages][${i}]=${kids[i]}` : `&children_ages[${i}]=${kids[i]}`;
            }
        }
        return url;
    }

    // build url for Additions
    makeAdditionsUrl(additions: number[], group: number | boolean = false) {
        let url = '';
        if (additions.length) {
            for(let i in additions) {
                url += (group !== false) ? `&rooms[${group}][additions][${i}]=${additions[i]}` : `&additions[${i}]=${additions[i]}`;
            }
        }
        return url;
    }

    makeMealsUrl(meals: number[], group: number | boolean = false) {
        let url = '';
        if (meals.length) {
            for(let i in meals) {
                url += (group !== false) ? `&rooms[${group}][meals][${i}]=${meals[i]}` : `&meals[${i}]=${meals[i]}`;
            }
        }
        return url;
    }

    makePeopleUrl(allPeople: { adults: number, kids: number, age: number[] }[] = []) {
        let url = '';
        for(let i in allPeople) {
            url += `${allPeople[i].adults}`;
            url += allPeople[i].age.length > 0 ? `:${(allPeople[i].age).join(',')}` : '';
            url += (parseInt(i) < allPeople.length - 1) ? '$' : '';
        }
        return url;
    }
}
