import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CurrencyPipe } from '@angular/common';

import { MatIconModule, MatInputModule, MatDatepickerModule, MatFormFieldModule, MatDialogModule, MatProgressSpinnerModule,
         MatButtonModule, MatNativeDateModule, MatMenuModule, MatSelectModule, MatTabsModule, MatExpansionModule,
         MatCheckboxModule, MatCardModule, MatProgressBarModule, MatTooltipModule, MatRadioModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './components/app.component';
import { HomeComponent } from './components/home.component';
import { NavtopComponent } from './components/nav-top.component';
import { NavTopPeopleComponent } from './components/nav-top-people.component';
import { NavTopPeopleRowComponent } from './components/nav-top-people-row.component';
import { NavTopRabatComponent } from './components/nav-top-rabat.component';
import { NavTopLanguageComponent } from './components/nav-top-language.component';
import { RoomsComponent } from './components/rooms.component';
import { RoomComponent } from './components/room.component';
import { PackagesComponent } from './components/packages.component';
import { GroupComponent } from './components/group.component';
import { ErrorComponent } from './components/error.component';
import { DetailDialogComponent } from './components/dialog-detail.component';
import { PackageDialogComponent } from './components/dialog-package.component';
import { PhotoDialogComponent } from './components/dialog-photo.component';
import { CalculationsDialogComponent } from './components/dialog-calculations.component';
import { RegulationsDialogComponent } from './components/dialog-regulations.component';
import { AdditionsComponent } from './components/additions.component';
import { AditionsStepsComponent } from './components/additions-steps.component';
import { SummaryCommonComponent } from './components/summary-common.component';
import { SummaryComponent } from './components/summary.component';
import { ConfirmationComponent } from './components/confirmation.component';
import { PaymentOnlineComponent } from './components/payment-online.component';
import { PaymentTransferComponent } from './components/payment-transfer.component';
import { PaymentBankComponent } from './components/payment-online-bank.component';

import { ParamsService } from './services/params.service';
import { ConfigService } from './services/config.service';
import { RestService } from './services/rest.service';
import { RoomsService } from './services/rooms.service';
import { PackagesService } from './services/packages.service';
import { AdditionsService } from './services/additions.service';
import { OtherService } from './services/other.service';
import { ReservationService } from './services/reservation.service';
import { UrlParamsService } from './services/urlparams.service';

import { KeysPipe } from './pipes/keys.pipe';
import { RoutingModule } from './routing.module';

@NgModule({
  declarations: [
      KeysPipe, AppComponent, NavtopComponent, NavTopPeopleComponent, NavTopPeopleRowComponent, NavTopRabatComponent,
      NavTopLanguageComponent, HomeComponent, RoomsComponent, RoomComponent, PackagesComponent, GroupComponent, ErrorComponent,
      DetailDialogComponent, PackageDialogComponent, CalculationsDialogComponent, PhotoDialogComponent, RegulationsDialogComponent,
      AdditionsComponent, AditionsStepsComponent, ConfirmationComponent, SummaryCommonComponent, SummaryComponent,
      PaymentOnlineComponent, PaymentTransferComponent, PaymentBankComponent
  ],
  entryComponents: [DetailDialogComponent, PackageDialogComponent, PhotoDialogComponent, CalculationsDialogComponent, RegulationsDialogComponent],
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule, RoutingModule, HttpClientModule, MatProgressBarModule, MatTooltipModule,
    MatIconModule, MatInputModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatMenuModule, MatDialogModule,
    BrowserAnimationsModule, MatSelectModule, MatButtonModule, MatTabsModule, MatCheckboxModule, MatCardModule, MatRadioModule,
    MatExpansionModule, MatProgressSpinnerModule
  ],
  providers: [ CurrencyPipe, UrlParamsService,
      ParamsService, ConfigService, RestService, RoomsService, PackagesService, AdditionsService, OtherService, ReservationService
  ],
  bootstrap: [ AppComponent ]
  //schemas:      [ NO_ERRORS_SCHEMA ] // custom tags
})

export class AppModule { }
